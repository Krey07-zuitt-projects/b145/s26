
db.users.find(
	{$or:[
		{"firstName":{$regex:"n",$options:"i"}},
		{"lastName":{$regex:"d",$options:"i"}}]
	},
	{
		"firstName":1,
		"lastName":1,
		"_id":0
	}
);

db.users.find( { $and: [ { "age": { $gte: 70 } }, { "department": "HR" } ] } );

db.users.find(
	{$and:[
		{"firstName":{$regex:"e",$options:"i"}},
		{"age":{$lte: 30}}]
	});
