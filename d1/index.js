
// find operation

	db.collections.find({query}, {field projection})

	// Query Operators

		// mini activity

			// look for a document/s that has an age of less than 50

	db.users.find (
			{
				"age":{$lt:50}
			}
		);
	// returned 1 document

	db.users.find (
			{
				"age":{$gte:50}
			}
		);
	// returned 3 documents


	// look for a document/s that has an age of not equal to 82
	db.users.find (

			{
				"age":{$ne:82}
			}
		)

	// look for a document/s that has names hawking and doe
	db.users.find(
		{
			"lastName":{$in:["Doe","Hawking"]}

		}
	);


	// courses
	db.users.find(
		{
			"courses":{$in:["React","HTML"]}

		}
	);

	// Logical Query Operators

	// INCORRECT *******
	db.users.find( {
		$or: [{firstName: {$in:["neil"]}, {"age":{$ne:25}}]
	});


	// CORRECT ********
	db.users.find ({
		$or: [{firstName: "Neil"},{age: 21}]
	})


	db.users.find ({
		$or: [{firstName: "Neil"},{age: {$gt:30}}]
	})


	db.users.find( { $and: [ { age: { $ne: 82 } }, { age: { $ne: 76 } } ] } );


	// FIELD PROJECTION

	db.users.find(
			{
				"firstName": "Jane"
			},
			{
				"firstName": 1,
				"lastName": 1,
				"contact": 1
			}
		);


	db.users.find(
		{
			"firstName":"Neil"
		},
		{
			"_id": 0,
			"firstName": 1,
			"lastName": 1,
			"contact": 1
		}
	)
	

	db.users.find(
		{
			"firstName":"bill"
		},
		{
			"_id": 1,
			"firstName": 1,
			"lastName": 1,
			"age": 1,
			"contact.phone": 1,
			"courses": 1,
			"department": 1
		}
	);

	// another solution
	db.users.find(
        {firstName: "Bill"},
        {
                "contact.email": 0
        }        
	);


	// Evaluation query operator

	// $regex operator

	db.users.find(
			{
				"firstName": {$regex: "N"}
			}
		);


	db.users.find(
			{
				"firstName": {$regex: "N"}
			}
		);

